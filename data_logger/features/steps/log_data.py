"""Testing feature for logging data."""

from behave import given, when, then
from data_logger import DataLogger


@given("that I have instantiate Data Logger")
def init_data_logger(context):
    """
    Scenarios:
        Record data
        Display data
        Log file
        Log file with multiple entries
    """
    context.data_logger = DataLogger()


@when("I pass some data to it")
def record_data(context):
    """
    Scenarios:
        Record data
    """
    context.data_logger.log("Message to be logged")


@when("I pass multiple data to it")
def record_multiple_data(context):
    """
    Scenarios:
        Log file with multiple entries
    """
    context.data_logger.log("Message to be logged")
    context.data_logger.log("Additional message to be logged")


@then("I should be able to obtain given data when needed")
def obtain_logged_data(context):
    """
    Scenarios:
        Record data
    """
    assert context.data_logger.data == ["Message to be logged"]


@then("I should be able to see given data when needed")
def display_logged_data(context):
    """
    Scenarios:
        Display data
    """
    assert context.data_logger.show() == ["Message to be logged"]


@then("data should be logged in the file")
def obtain_log_file(context):
    """
    Scenarios:
        Log file
    """
    assert read_file("data.log") == "Message to be logged\n"


@then("all data should be logged in the same file")
def obtain_log_file_with_multiple_entries(context):
    """
    Scenarios:
        Log file with multiple entries
    """
    message = "Message to be logged\n"
    message += "Additional message to be logged\n"
    assert read_file("data.log") == message


def read_file(filename):
    "Read file content."
    data = ""
    with open(filename, "r", encoding="utf8") as file:
        data = file.read()
    return data
