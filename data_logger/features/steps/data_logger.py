"""Data Logger implementation."""


class DataLogger:
    """Data Logger implementation."""

    def __init__(self, file_name="data.log"):
        self._data = []
        self._file_name = file_name
        self._init_file()

    @property
    def data(self):
        """Getter for data."""
        return self._data

    def flush(self, data, write_cmd):
        """Write log data to the file."""
        with open(self._file_name, write_cmd, encoding="utf8") as log_file:
            log_file.write(data)
            log_file.close()

    def _init_file(self):
        """Create a log file."""
        self.flush("", "w")

    def log(self, data):
        """Logging provided data."""
        self._data.append(data)
        self.flush(data + "\n", "+a")

    def show(self):
        """Render logged data."""
        return self._data
