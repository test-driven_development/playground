Feature: Logging provided data
  As a data logger user, I want to record data using data logger so that I can capture states of my source code during execution.

  Scenario: Record data
    Given that I have instantiate Data Logger 
    When I pass some data to it
    Then I should be able to obtain given data when needed

  Scenario: Display data
    Given that I have instantiate Data Logger 
    When I pass some data to it 
    Then I should be able to see given data when needed

  Scenario: Log file
    Given that I have instantiate Data Logger 
    When I pass some data to it 
    Then data should be logged in the file 

  Scenario: Log file with multiple entries
    Given that I have instantiate Data Logger 
    When I pass multiple data to it
    Then all data should be logged in the same file